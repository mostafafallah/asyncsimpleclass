﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsyncSampleProject
{
    internal class AsyncSyncDiffer_B
    {
        internal static void Start()
        {
            callMethod();
            Console.ReadKey();
        }

        static async void callMethod()
        {
            Task<int> task = Method1();
            Method2();
            int count = await task;
            Method3(count);
        }

        static async Task<int> Method1()
        {
            int count = 0;
            await Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    Console.WriteLine(" Method 1");
                    count += 1;
                }
            });
            return count;
        }


        static void Method2()
        {
            for (int i = 0; i < 50; i++)
            {
                Console.WriteLine(" Method 2");
            }
        }


        public static void Method3(int count)
        {
            Console.WriteLine("Total count is " + count);
        }
    }

}
