﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsyncSampleProject
{
    internal class AsyncSyncDiffer_A
    {
        internal static void Start()
        {
            Method1();
            Method2();
            Console.ReadKey();
        }

        static async Task Method1()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(" Method 1");
                    Console.ResetColor();
                }
            });
        }


        static void Method2()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(" Method 2");
                Console.ResetColor();
            }
        }
    }

}
