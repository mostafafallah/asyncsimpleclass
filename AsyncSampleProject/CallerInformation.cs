﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace AsyncSampleProject
{
    internal class CallerInformation
    {
        internal static void Start()
        {
            Console.WriteLine("Main method Start");
            MyMethodB();
            MyMethodA();
            Console.WriteLine("Main method End!");
            Console.ReadLine();
        }

        static void MyMethodA()
        {
            MyMethodB();
        }

        static void MyMethodB([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
        {
            InsertLog(memberName);
        }

        static void InsertLog(string method)
        {
            Console.WriteLine("{0} called MyMethodB at {1}", method, DateTime.Now);
        }
    }
}
